#include "DBUtils.h"

namespace DBUtils {
	/*
		Method used to connect the server to the MYSQL database
		-------------------------------------------------------
		Upon establishing a successful connection, the 'connected' bool
		will be set to true
		@PARAM host, sql server hsot
		@PARAM user, username
		@PARAM pw, password for user
		@PARAM db, name of the database to connect to
	*/
	void connect(std::string host, std::string user, std::string pw, std::string db) {
		database = db;
		// Attempt connection to SQL server
		try {
			// Create a connection
			driver = get_driver_instance();
	  		con = driver->connect(host, user, pw);
			// Connect to the MySQL test database
			con->setSchema(db);
			std::cout << "Connected to '" << host << "/" << database << "' database successfully." << std::endl;
			connected = true;
		} catch (sql::SQLException &e) {
			std::cout 	<< "# SQL CONNECTION ERR: " << e.what()
						<< ", (@DBUtils::" << __FUNCTION__ << std::endl;
		}
	}

	/*
		Method used to set the connection state of a tracker to false
		-------------------------------------------------------
		The tracker is located from the current port number -> tracker ID relation
		@PARAM port, the port number to set to inactive
	*/
	void set_connection_to_inactive(int port) {
	    try {
	    	sql::Statement 	*stmt;
	        // Execute query
	        std::ostringstream db_query;
	        db_query << "UPDATE `" << database << "` t" << std::endl
	                 << "SET t.port = NULL, t.connection_active = false, t.session_packet_transmission_size = 0, t.session_start = NULL" << std::endl
	                 << "WHERE t.port = " << port << ";";
	        std::string sql_query = db_query.str();
	        stmt = con->createStatement();
	        stmt->execute(sql_query.c_str());
	    } catch (sql::SQLException &e) {
	        std::cout 	<< "# SQL EXECUTE ERR: " << e.what()
	        			<< ", (@ DBUtils::" << __FUNCTION__ << std::endl;
	    }
	}

	/*
		Method used to set the status of all trackers to inactive
		-------------------------------------------------------
		@OUTPUT success bool
	*/
	bool set_all_connections_to_inactive() {
	    try {
	    	sql::Statement 	*stmt;
	        // Execute query
	        std::ostringstream db_query;
	        db_query << "UPDATE `" << database << "` t" << std::endl
	                 << "SET t.port = NULL, t.connection_active = false, t.session_packet_transmission_size = 0, t.session_start = NULL" << std::endl
	                 << "WHERE t.port != ''";
	        std::string sql_query = db_query.str();
	        stmt = con->createStatement();
	        stmt->execute(sql_query.c_str());
	        return true;
	    } catch (sql::SQLException &e) {
	        std::cout 	<< "# SQL EXECUTE ERR: " << e.what()
	        			<< ", (@ DBUtils::" << __FUNCTION__ << std::endl;
			return false;
	    }
	}

	/* Method to get details on the supplied connection port
	----------------------------------------------------------
	Returns the port if nothing is found
	@PARAMS port, port number to request details of
	*/
	std::string get_port_description(int port) {
		std::ostringstream out;
		out << "(Port " << port << ")";
		return out.str();
	}

	/* Method used for inserting a new PACKET entry into the database
	---------------------------------------------------------
		@PARAMS inputs, string array of inputs
	*/
	void insert_packet(std::string line, std::string inputs[], int port) {
		// Assign to variables
		// <tracker_id>,<lat>,<lng>,<hAcc>,<battery_volt>,<alarm_state>
		std::string		trackerID 				= inputs[0],
						latitude 				= process_latlng(inputs[1]),
						longitude 				= process_latlng(inputs[2]),
						horizontal_accuracy		= inputs[3],
						battery_level			= inputs[4],
						alarm_state				= inputs[5];
		// Display packet contents
		std::cout 	<< "(PKT) ID: " << trackerID << ", Lat: " << latitude << ", Long: "
					<< longitude << ", hAcc: " << horizontal_accuracy << ", Bat Volt: "
					<< battery_level << ", Alarm: " << alarm_state << " (Port: " << port << ")" << std::endl;
		try {
			// Add entry to db
			sql::Statement 	*stmt;
			// Execute query
		    std::ostringstream db_query;
			db_query 	<< "INSERT INTO `received_packets` (tracker_id, latitude, longitude, horizontal_accuracy, battery_voltage, alarm_status, packet, port)" << std::endl
				 		<< "VALUES (" << trackerID << ",\'" << latitude << "\',\'" <<  longitude <<"\',\'" << horizontal_accuracy << "\',\'" << battery_level << "\',\'" << alarm_state << "\',\'" << line << "\',\'" << port << "\');";
			std::string sql_query = db_query.str();
			stmt = con->createStatement();
			stmt->execute(sql_query.c_str());
		} catch (sql::SQLException &e) {
			std::cout 	<< "# ERR: SQLException in " << __FILE__
						<< "(" << __FUNCTION__ << ") on line "
						<< __LINE__ << std::endl
						<< "# ERR: " << e.what()
						<< " (MySQL error code: " << e.getErrorCode()
						<< ", SQLState: " << e.getSQLState() << " )"
						<< " Packet: " << line << std::endl;
		}
	}


	/*
		Used to add a decimal point at after 2 integer places
		------------------------------------------
		@PARAM in, lat/lng value
		@OUTPUT lat/lng value with decimal point
	*/
	std::string process_latlng(std::string in) {
		// Change (-)xxxxxxx to -xx.xxxxxx, in case of empty string, return null string.
		if (!in.empty() && in.length() > 3) return in[0] == '-' ? in.insert(3,".") : in.insert(2,".");
		else return std::string();
	}
};