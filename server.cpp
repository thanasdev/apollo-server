/* For sockaddr_in */
#include <netinet/in.h>
/* For socket functions */
#include <sys/socket.h>
/* For fcntl */
#include <fcntl.h>

/* SSL includes */
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>

#include <arpa/inet.h>
#include <event2/event.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>
#include <event2/bufferevent_ssl.h>
#include <event2/listener.h>

/* Standard C++ includes */
#include <time.h>
#include <ios>
#include <fstream>
#include <iostream>
#include <sstream>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <regex>

#include "DBUtils.h" // Utility class for MySQL

#define MAX_LINE 	 	16384
#define _PORT		 	40713
#define _DB			 	"tracker"
#define _PKT_LENGTH 	6
#define _PKT_PATTERN 	"^([1-9]{1,16},)([-]{0,1}[0-9]{0,16},){3}([0-9]{0,},){2}$"
#define _MAX_QUEUE		16
#define _TIMEOUT_SEC 	120

void do_read(evutil_socket_t fd, short events, void *arg);
void do_write(evutil_socket_t fd, short events, void *arg);

/*
	Used to split the packet by ',' delimeter
	------------------------------------------
	@PARAM line, read buffer input
	@PARAM in, std::string array to be returned with split elements
*/
void process_pkt (char * line, std::string (&in)[_PKT_LENGTH]) {
	int i = 0;

	std::stringstream ss(line);
	std::string item;

	while (getline(ss, item, ',') && i < _PKT_LENGTH) {
		in[i++] = item;
	}
}

/*
	Callback method used to read, verify and insert buffer inputs
	-----------------------------------------
	@PARAM *bev, input bufferevent struct
*/
void readcb(struct bufferevent *bev, void *ctx) {
	struct evbuffer *input;
	char 			*line;
	std::string 	inputs[_PKT_LENGTH];
	size_t 			n;
	int 			port = bufferevent_getfd(bev);

	// Get the received packet
	input 	= bufferevent_get_input(bev);
	line 	= evbuffer_readln(input, &n, EVBUFFER_EOL_LF);

	// Ensure the packet is a valid string
	if (line == nullptr || std::string(line).empty()) return;

	// Verify the packet being received is in the correct format
	std::regex e(_PKT_PATTERN);
	if (!std::regex_match(line, e)) {
		std::cout << DBUtils::get_port_description(port) << " ERR: Invalid packet received: " << line << std::endl;
		// Insert into DB (for logging purposes)
		DBUtils::insert_packet(line, port);
	} else {
		// Split the packet accordingly
		process_pkt(line, inputs);
		// Insert into DB
		DBUtils::insert_packet(line, inputs, port);
	}
	// Free held memory
    free(line);
}


/*
	Callback method used for handling error events
	---------------------------------------------------
	Sets the trackers port to inactive on an error event
	@PARAM bev, incoming buffer event
	@PARAM error, error code
	@PARAM ctx, listener event
*/
void errorcb(struct bufferevent *bev, short error, void *ctx) {
	int port = 	bufferevent_getfd(bev);

	if (error && error != 128) { // skip key revocation error
		// Get the description of the error from the errno
		std::cout << DBUtils::get_port_description(port) << " ERR: #" << error;
		// Display further information
	    if (BEV_EVENT_TIMEOUT) std::cout << " (client timed out)" << std::endl;
	    else if (BEV_EVENT_ERROR) std::cout << " (" << evutil_socket_error_to_string(error) << ")" << std::endl;
		// Set the port to inactive
		DBUtils::set_connection_to_inactive(port);
    }
	bufferevent_free(bev);
}

/*
	Method used to accept new bufferevents
	---------------------------------------------------
	@PARAMS listener, primary fd
	@PARAMS event, event identifier
	@PARAMS arg, void cast event_base
*/
void do_accept(evutil_socket_t listener, short event, void *arg) {
    struct event_base 		*base = (event_base*)arg;
    struct sockaddr_storage ss;
    socklen_t 				slen = sizeof(ss);
    struct timeval 			tv;

    // Set connection timeout
  	tv.tv_sec = _TIMEOUT_SEC;
  	tv.tv_usec = 0;

    // Get the file descriptor for the new client
    int fd = accept(listener, (struct sockaddr*)&ss, &slen);

    if (fd < 0) perror("accept");
    else if (fd > FD_SETSIZE) close(fd);
    else {
		std::cout << "New client on port: " << fd << std::endl;
        struct bufferevent *bev;
        evutil_make_socket_nonblocking(fd);
        bev = bufferevent_socket_new(base, fd, BEV_OPT_CLOSE_ON_FREE);
        // Add timeout to event listener
        bufferevent_set_timeouts(bev, &tv, NULL);
        bufferevent_setcb(bev, readcb, NULL, errorcb, NULL);
        bufferevent_setwatermark(bev, EV_READ, 0, MAX_LINE);
        bufferevent_enable(bev, EV_READ|EV_WRITE);
    }
}

void run(void) {
	evutil_socket_t 	listener;
//	SSL_CTX *ctx;
//	struct evconnlistener *listener;
	struct sockaddr_in 	sin;
	struct event_base 	*evbase;
	struct event 		*listener_event;

	// Set disconnect state on all trackers
	if (!DBUtils::set_all_connections_to_inactive()) return;

	if (!(evbase = event_base_new())) return;

	// Setup server address info
	memset(&sin, 0, sizeof(sin));
	sin.sin_family 			= AF_INET; // IPv4
	sin.sin_addr.s_addr 	= 0;
	sin.sin_port 			= htons(_PORT);

	// Get listener file descriptor
	if (!(listener = socket(AF_INET, SOCK_STREAM, 0))) return;

	// Ensure the port/listener is asynchronous
	evutil_make_socket_nonblocking(listener);

	
	// Ensure the port is open
#ifndef WIN32
    {
        int one = 1;
        setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
    }
#endif
    // Bind to port
    if (bind(listener, (struct sockaddr*)&sin, sizeof(sin)) < 0) {
		perror("bind");
        return;
    } else std::cout << "Succesfully bound to port " << _PORT << std::endl;

    // Start listening
    if (listen(listener, _MAX_QUEUE) < 0) {
		perror("listen");
        return;
    } else std::cout << "Listening..." << std::endl;

    listener_event = event_new(	evbase,
    							listener,
    							EV_READ | EV_PERSIST,
    							do_accept,
    							(void*)evbase);
    
    // Add listener to event base
    event_add(listener_event, NULL);

    // Set the listener to active
    event_base_dispatch(evbase);
}

int main(int c, char **v) {
	// Make the connection to the database
	DBUtils::connect("tcp://127.0.0.1:3306", "server", "xCFvva3#", _DB);
	// Ensure database is connected before proceeding
	if (DBUtils::connected) {
		setvbuf(stdout, NULL, _IONBF, 0);
    	run();
	}

	return 0;
}



//	ctx = evssl_init();
//	if (ctx == NULL) return; // err //

	
/*	listener = evconnlistener_new_bind(
					evbase, ssl_acceptcb, (void *)ctx,
					LEV_OPT_CLOSE_ON_FREE | LEV_OPT_REUSEABLE, 1024,
					(struct sockaddr *)&sin, sizeof(sin));

	if (listener != NULL)
		std::cout << "Bound to port: " << _PORT << std::endl;
	event_base_loop(evbase, 0);
	evconnlistener_free(listener);
	SSL_CTX_free(ctx);
*/
	
/*static void ssl_acceptcb(struct evconnlistener *serv, int sock, struct sockaddr *sa, int sa_len, void *arg) {
	struct event_base 	*evbase;
	struct bufferevent 	*bev;
	SSL_CTX 		*server_ctx;
	SSL 			*client_ctx;

	server_ctx = (SSL_CTX *)arg;
	client_ctx = SSL_new(server_ctx);
	evbase = evconnlistener_get_base(serv);

	bev = bufferevent_openssl_socket_new(evbase, sock, client_ctx,
						BUFFEREVENT_SSL_ACCEPTING,
						BEV_OPT_CLOSE_ON_FREE);
	int fd = bufferevent_getfd(bev);
	bufferevent_setcb(bev, readcb, NULL, errorcb, NULL);
	bufferevent_enable(bev, EV_READ|EV_WRITE);
	std::cout << "New client on port: " << fd << std::endl;
}*/
/*
static SSL_CTX * evssl_init(void) {
	SSL_CTX  *server_ctx;

	// Initialize the OpenSSL library
	SSL_load_error_strings();
	SSL_library_init();
	// We MUST have entropy, or else there's no point to crypto.
	if (!RAND_poll()) return NULL;

	server_ctx = SSL_CTX_new(SSLv23_server_method());

	if (! SSL_CTX_use_certificate_chain_file(server_ctx, "cert") ||
	! SSL_CTX_use_PrivateKey_file(server_ctx, "pkey", SSL_FILETYPE_PEM)) {
		std::cout << "Couldn't read 'pkey' or 'cert' file.  To generate a key\n"
		<< "and self-signed certificate, run:\n"
		<< "  openssl genrsa -out pkey 2048\n"
		<< "  openssl req -new -key pkey -out cert.req\n"
		<< "  openssl x509 -req -days 365 -in cert.req -signkey pkey -out cert" << std::endl;
		return NULL;
	}
	SSL_CTX_set_options(server_ctx, SSL_OP_NO_SSLv2);

	return server_ctx;
}*/