CC=gcc
CXX=g++
RM=rm -f
CPPFLAGS=-g -Wall -Werror -std=c++11
LDFLAGS=-g -L. -L/usr/local/ssl/lib -L/usr/local/libevent2/lib
LDLIBS=-levent -levent_core -lmysqlcppconn -levent_openssl -lssl -lcrypto

NAME=Server
SRCS=server.cpp
OBJS=$(subst .cpp,.o,$(SRCS))

all: $(NAME)

$(NAME): $(OBJS)
	$(CXX) $(LDFLAGS) -o $(NAME) $(OBJS) $(LDLIBS) 

depend: .depend

.depend: $(SRCS)
	rm -f ./.depend
	$(CXX) $(CPPFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS)

sync:
	./utils/sync-remote.sh;

dist-clean: clean
	$(RM) *~ .depend

include .depend
