#!/bin/bash
echo "--* STARTING 'log.sh' *--" | cat - /tcp_server/tcp_server.log > /tcp_server/.tmp/out && mv /tcp_server/.tmp/out /tcp_server/tcp_server.log

while read line ; do
    echo "[$(date)]   ${line}" | cat - /tcp_server/tcp_server.log > /tcp_server/.tmp/out && mv /tcp_server/.tmp/out /tcp_server/tcp_server.log
#    head -n 1000 /tcp_server/tcp_server.log > /tcp_server/.tmp/out && mv /tcp_server/.tmp/out /tcp_server/tcp_server.log
done
